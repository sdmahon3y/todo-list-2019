import React from 'react';
import './App.css';

class App extends React.Component {
	setCurrentToDoItem = (toDoItem) => {
		console.log('toDoItem');
		
		this.setState({
			currentToDoItem: toDoItem
		});
	};
	
	saveToDoListItem = (toDoItem) => {
		this.setState({
			toDoList: [
				...this.state.toDoList,
				toDoItem
			]
		});
	};
	
	constructor(props) {
		super(props);
		
		this.state = {
			currentToDoItem: null,
			showHideStrikethrough: "hidden",
			toDoList: []
		};
		
		this.toggleStrikethrough = this.toggleStrikethrough.bind(this);
	}

	toggleStrikethrough() {
		alert("Clicked");
		//alert(this.state.showHideStrikethrough);
		let css = (this.state.showHideStrikethrough === "hidden") ? "show" : "hidden";
		this.setState({
			showHideStrikethrough: css 
		});
		}
	
	render() {
  		return (
  			<div>
				<h1>To Do List</h1>
	  			<label>To Do Item: </label>
				<input
	  				onChange={(event) => this.setCurrentToDoItem(event.target.value)}
	  			>
			
	  			</input>
				<button
					onClick={() => this.saveToDoListItem(this.state.currentToDoItem)}
				>
					<p>Add Item</p>
				</button>
				
				<p>{this.state.currentToDoItem}</p>

				<div>
					<p>To Do Items</p>
					{
						this.state.toDoList.map((item) => <p onClick={this.toggleStrikethrough} className={this.state.showHideStrikethrough} key={item}>{item}</p>)
					}
				</div>
  			</div>
  		);
	}
}

	 

export default App;

/*
to do list
input with label
show the to do items
 */
